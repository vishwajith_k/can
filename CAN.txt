Brief history:
Development of the CAN bus started in 1983 at Robert Bosch GmbH. The protocol was officially released in 1986 at the Society of Automotive Engineers (SAE) conference in Detroit, Michigan. The first CAN controller chips, produced by Intel and Philips, came on the market in 1987. Released in 1991, the Mercedes-Benz W140 was the first production vehicle to feature a CAN-based multiplex wiring system.
Bosch published several versions of the CAN specification and the latest is CAN 2.0 published in 1991. This specification has two parts; part A is for the standard format with an 11-bit identifier, and part B is for the extended format with a 29-bit identifier. These standards are freely available from Bosch along with other specifications and white papers.
In 1993, CAN was standardised with ISO 11898, ISO 11898-1 (data link layer), and ISO 11898-2 (physical layer) for high-speed CAN. ISO 11898-3 (physical layer) for low-speed, fault-tolerant CAN. The physical layer standards ISO 11898-2 and ISO 11898-3 are not part of the Bosch CAN 2.0 specification. These standards may be purchased from the ISO.

11-bit identifier -> CAN 2.0A
29-bit identifier -> CAN 2.0B

Introduction:
A Controller Area Network (CAN bus) is a robust vehicle bus standard designed to allow microcontrollers and devices to communicate with each others' applications without a host computer. It is a message-based protocol, designed originally for multiplex electrical wiring within automobiles to save on copper, but can also be used in many other contexts. For each device the data in a packet is transmitted sequentially but in such a way that if more than one device transmits at the same time the highest priority device is able to continue while the others back off. Packets are received by all devices, including by the transmitting device. The BOSCH's CAN is a multi-master, message broadcast system that specifies a maximum signaling rate of 1 megabit per second (bps). Unlike a traditional network such as USB or Ethernet, CAN does not send large blocks of data point-to-point from node A to node B under the supervision of a central bus master.

Applications:
Automotive: Auto start/stop, Electric park brakes, Parking assist systems, Auto lane assist/collision avoidance systems, Auto brake wiping
Agricultural equipment
Electronic equipment for aviation and navigation
Industrial automation and mechanical control
Elevators, escalators
Building automation
Medical instruments and equipment
Pedelecs
Robotics
Prosthetic arm

Architecture:
[CAN Arch.png]
[CAN Arch - controller level.png]

Architecture - The CAN Bus:
The High-Speed ISO 11898 Standard specifications are given for a maximum signaling rate of 1 Mbps with a bus length of 40 m with a maximum of 30 nodes. The cable is specified to be a shielded or unshielded twisted-pair with a 120-Ω characteristic impedance(Zo).The ISO 11898 Standard defines a single line of twisted-pair cable as the network topology, terminated at both ends with 120-Ω resistors, which match the characteristic impedance of the line to prevent signal reflections. According to ISO 11898, placing RL on a node must be avoided because the bus lines lose termination if the node is disconnected from the bus.
According to CAN terminology, Recessive is similar to logic high and Dominant similar to logic low. Dominant takes over recessive of other node in identification part of framing. The two signal lines of the bus, CANH and CANL, in the recessive state, are passively biased to ≉ 2.5 V. The dominant state on the bus takes CANH ≉ 1 V higher to ≉ 3.5 V, and takes CANL ≉ 1 V lower to ≉ 1.5 V, creating a typical 2-V differential signal.
[CAN Bus.png]
[CAN Electrical.png]

Architecture - Nodes:
Each node has a CPU/microprocessor/host processor, CAN Controller, Transceiver. Typically CAN controller is integral part of host. The host processor decides what the received messages mean and what messages it wants to transmit. Sensors, actuators and control devices can be connected to the host processor.
Receiving: the CAN controller stores the received serial bits from the bus until an entire message is available, which can then be fetched by the host processor (usually by the CAN controller triggering an interrupt).
Sending: the host processor sends the transmit message/s to a CAN controller, which transmits the bits serially onto the bus when the bus is free.
Tranceiver protects CAN circuitary during reception and converts data stream to CAN bus levels during transmission.
[CAN Node.png]

Architecture - Framing:
The data frame is the only frame for actual data transmission. There are two message formats:
Base/Standard frame format: with 11 identifier bits
Extended frame format: with 29 identifier bits
The CAN standard requires the implementation must accept the base frame format and may accept the extended frame format, but must tolerate the extended frame format. CAN frame contains Arbitration field, control bits, data bits, CRC field, ACK field, EOF field. Depending on type of frame repeatation of fields and bits in fields will vary.

Architecture - Framing - Base CAN frame format:

[Base CAN frame.png]

SOF - The single dominant start of frame (SOF) bit marks the start of a message, and is used to synchronize the nodes on a bus after being idle.

Identifier-The Standard CAN 11-bit identifier establishes the priority of the message. The lower the binary value, the higher its priority.

RTR - The single remote transmission request (RTR) bit is dominant when information is required from another node. All nodes receive the request, but the identifier determines the specified node. The responding data is also received by all nodes and used by any node interested. In this way, all data being used in a system is uniform.

IDE - A dominant single identifier extension (IDE) bit means that a standard CAN identifier with no extension is being transmitted.

r0 - Reserved bit (for possible use by future standard amendment).

DLC - The 4-bit data length code (DLC) contains the number of bytes of data being transmitted.

Data - Up to 64 bits of application data may be transmitted.

CRC - The 16-bit (15 bits plus delimiter) cyclic redundancy check (CRC) contains the checksum (number of bits transmitted) of the preceding application data for error detection.

ACK - Every node receiving an accurate message overwrites this recessive bit in the original message with a dominate bit, indicating an error-free message has been sent. Should a receiving node detect an error and leave this bit recessive, it discards the message and the sending node repeats the message after rearbitration. In this way, each node acknowledges (ACK) the integrity of its data. ACK is 2 bits, one is the acknowledgment bit and the second is a delimiter. EOF - This end-of-frame (EOF), 7-bit field marks the end of a CAN frame (message) and disables bit-stuffing, indicating a stuffing error when dominant. When 5 bits of the same logic level occur in succession during normal operation, a bit of the opposite logic level is stuffed into the data.

IFS - This 7-bit interframe space (IFS) contains the time required by the controller to move a correctly received frame to its proper position in a message buffer area.

Architecture - Framing - Standard CAN frame format:

[Extended CAN frame.png]

SRR - The substitute remote request (SRR) bit replaces the RTR bit in the standard message location as a placeholder in the extended format.

IDE - A recessive bit in the identifier extension (IDE) indicates that more identifier bits follow. The 18-bit extension follows IDE.

r1 - Following the RTR and r0 bits, an additional reserve bit has been included ahead of the DLC bit.

CAN Messaging:
The allocation of priority to messages in the identifier is a feature of CAN that makes it particularly attractive for use within a real-time control environment. The lower the binary message identifier number, the higher its priority. An identifier consisting entirely of zeros is the highest priority message on a network because it holds the bus dominant the longest. Therefore, if two nodes begin to transmit simultaneously, the node that sends a last identifier bit as a zero (dominant) while the other nodes send a one (recessive) retains control of the CAN bus and goes on to complete its message. A dominant bit always overwrites a recessive bit on a CAN bus. The allocation of message priority is up to a system designer, but industry groups mutually agree on the significance of certain messages.
Typically, a logic-high is associated with a one, and a logic-low is associated with a zero - but not so on a CAN bus. This is why many CAN transceivers have the driver input and receiver output pins passively pulled high internally, so that in the absence of any input, the device automatically defaults to a recessive bus state on all input and output pins.

CAN Messaging - Arbitration:
Bus access is event-driven and takes place randomly. If two nodes try to occupy the bus simultaneously, access is implemented with a nondestructive, bit-wise arbitration. Nondestructive means that the node winning arbitration just continues on with the message, without the message being destroyed or corrupted by another node.
Note that a transmitting node constantly monitors each bit of its own transmission. This is the reason for the transceiver configuration in which the CANH and CANL output pins of the driver are internally tied to the receiver's input. The propagation delay of a signal in the internal loop from the driver input to the receiver output is typically used as a qualitative measure of a CAN transceiver. This propagation delay is referred to as the loop time.
[Arbitration - Inverted Logic of CAN.png]

CAN Messaging - An Example:
[CAN Arbitration - Example.png]

CAN Messaging - Message Types:
1. Data Frame: The Arbitration Field contains an 11-bit identifier in standard frame and the RTR bit, which is dominant for data frames. In Extended Frame, it contains the 29-bit identifier and the RTR bit. Next is the Data Field which contains zero to eight bytes of data, and the CRC Field which contains the 16-bit checksum used for error detection. Last is the Acknowledgment Field.

2. Remote Frame: The intended purpose of the remote frame is to solicit the transmission of data from another node. The remote frame is similar to the data frame, with two important differences. First, this type of message is explicitly marked as a remote frame by a recessive RTR bit in the arbitration field, and secondly, there is no data.

3. Error Frame: The error frame is a special message that violates the formatting rules of a CAN message. It is transmitted when a node detects an error in a message, and causes all other nodes in the network to send an error frame as well. The original transmitter then automatically retransmits the message. An elaborate system of error counters in the CAN controller ensures that a node cannot tie up a bus by repeatedly transmitting error frames.

4. Overload Frame: The overload frame is mentioned for completeness. It is similar to the error frame with regard to the format, and it is transmitted by a node that becomes too busy. It is primarily used to provide for an extra delay between messages.

Bit Stuffing:
To ensure enough transitions to maintain synchronization, a bit of opposite polarity is inserted after five consecutive bits of the same polarity. This practice is called bit stuffing, and is necessary due to the non-return to zero (NRZ) coding used with CAN. The stuffed data frames are destuffed by the receiver.
Bit stuffing means that data frames may be larger than one would expect by simply enumerating the bits shown in the tables above. The maximum increase in size of a CAN frame (base format) after bit stuffing is in the case

    11111000011110000...

which is stuffed as:

    111110000011111000001...

The stuffing bit itself may be the first of the five consecutive identical bits, so in the worst case there is one stuffing bit per four original bits.

Bit Stuffing - Exceptions:
All fields in the frame are stuffed with the exception of the CRC delimiter, ACK field and end of frame which are a fixed size and are not stuffed. In the fields where bit stuffing is used, six consecutive bits of the same polarity (111111 or 000000) are considered an error. An active error flag can be transmitted by a node when an error has been detected. The active error flag consists of six consecutive dominant bits and violates the rule of bit stuffing.
An undesirable side effect of the bit stuffing scheme is that a small number of bit errors in a received message may corrupt the destuffing process, causing a larger number of errors to propagate through the destuffed message. This reduces the level of protection that would otherwise be offered by the CRC against the original errors.

Errors:
The robustness of CAN may be attributed in part to its abundant error-checking procedures. The CAN protocol incorporates five methods of error checking: three at the message level and two at the bit level. If a message fails any one of these error detection methods, it is not accepted and an error frame is generated from the receiving node. This forces the transmitting node to resend the message until it is received correctly. However, if a faulty node hangs up a bus by continuously repeating an error, its transmit capability is removed by its controller after an error limit is reached.
Error checking at the message level is enforced by the CRC and the ACK slots. The 16-bit CRC contains the checksum of the preceding application data for error detection with a 15-bit checksum and 1-bit delimiter. The ACK field is two bits long and consists of the acknowledge bit and an acknowledge delimiter bit.
Also at the message level is a form check. This check looks for fields in the message which must always be recessive bits. If a dominant bit is detected, an error is generated. The bits checked are the SOF, EOF, ACK delimiter, and the CRC delimiter bits.
At the bit level, each bit transmitted is monitored by the transmitter of the message. If a data bit (not arbitration bit) is written onto the bus and its opposite is read, an error is generated. The only exceptions to this are with the message identifier field which is used for arbitration, and the acknowledge slot which requires a recessive bit to be overwritten by a dominant bit.
The final method of error detection is with the bit-stuffing rule where after five consecutive bits of the same logic level, if the next bit is not a complement, an error is generated. Stuffing ensures that rising edges are available for on-going synchronization of the network. Stuffing also ensures that a stream of bits are not mistaken for an error frame, or the seven-bit interframe space that signifies the end of a message. Stuffed bits are removed by a receiving node’s controller before the data is forwarded to the application.
With this logic, an active error frame consists of six dominant bits—violating the bit stuffing rule. This is interpreted as an error by all of the CAN nodes which then generate their own error frame. This means that an error frame can be from the original six bits to twelve bits long with all the replies. This error frame is then followed by a delimiter field of eight recessive bits and a bus idle period before the corrupted message is retransmitted. It is important to note that the retransmitted message still has to contend for arbitration on the bus.

Errors - Types of Errors:

1. Bit Errors: Occurs when the bit transmitted is different from received bit OR change of bit/s anywhere in the frame refers to bit error. This type of error can be detected by monitoring the bus while transmitting. But this kind of detection is not required during arbitration phase, ACK phase or while sending a passive error frame.

2. Bit Stuffing Errors: Error caused when 6/more consecutive bits of same polarity are received is named as bit stuffing error. These errors are detected by receiver. Main reasons for this type of error are, improper bit-stuffing while transmission, bit inversion on the channel, transmission of active error frame. This error does not count on ACK fields, EOF field, CRC delimiter and even interframe space.

3. CRC Errors: This type error occurs when receiver calculated CRC is different from CRC on frame. Hamming distance of CRC error detection is 6 per frame.

4. Message Format Errors OR Form Errors: Form errors are observed by receiver when the message on CAN bus is not according to format or frame fields OR dilution of frame integrity causes receiver to detect form error. Causes for this type of error include transmission of active error frame during EOF field, transmission of overload field during IFS (Interframe space), transceiver error in transmission.

5. Acknowledgment OR ACK Errors: Occurs when ACK bit is recessive. Noticed by sender when there's recessive bit on CAN bus after it has transmitted recessive bit during ACK state. This error tells sender that, either there's no other receiver on bus or received frame is likely to be errorneous.

Errors - Error States: Detected errors are made public to all other nodes via error frames or error flags. The transmission of an erroneous message is aborted and the frame is repeated as soon as the message can again win arbitration on the network. Also, each node is in one of three error states, error-active, error-passive or bus-off.

Errors - Error States - Error Active: An error-active node can actively take part in bus communication, including sending an active error flag, which consists of six consecutive dominant bits. The error flag actively violates the bit stuffing rule and causes all other nodes to send an error flag, called the error echo flag, in response. An active error flag,and the subsequent error echo flag may cause as many as twelve consecutive dominant bits on the bus; six from the active error flag, and zero up to six more from the error echo flag depending upon when each node detects an error on the bus. A node is error-active when both the Transmit Error Counter (TEC)and the Receive Error Counter (REC) are below 128. Error-active is the normal operational mode, allowing the node to transmit and receive without restrictions.

Errors - Error States - Error Passive: A node becomes error-passive when either the transmit error counter or receive error counter exceeds 127. Error-passive nodes are not permitted to transmit active error flags on the bus, but instead, transmit passive error flags which consist of six recessive bits. If the error-passive node is currently the only transmitter on the bus then the passive error flag will violate the bit stuffing rule and the receiving node/s will respond with error flags of their own (either active or passive depending upon their own error state). If the error-passive node in question is not the only transmitter (i.e. during arbitration) or is a receiver, then the passive error flag will have no effect on the bus due to the recessive nature of the error flag. When an error-passive node transmits a passive error flag and detects a dominant bit, it must see the bus as being idle for eight additional bit times after an intermission before recognizing the bus as available. After this time, it will attempt to retransmit.

Errors - Error States - Bus Off: A node goes into the bus-off state when the TEC is greater than 255 (receive error scan not cause a node to go bus-off). In this mode, the node can not send or receive messages, acknowledge messages, or transmit error frames of any kind. This is how fault confinement is achieved. There is a bus recovery sequence that is defined by the CAN protocol that allows a node that is bus-off to recover, return to error-active, and begin transmitting again if the fault condition is removed.

CAN Bit Time: The CAN bit time is made up of non-overlapping segments. Each of these segments are made up of integer units called Time Quanta (TQ). The Nominal Bit Rate (NBR) is defined in the CAN specification as the number of bits per second transmitted by an ideal transmitter with no resynchronization and can be described with the equation NBR = fbit = 1 / tbit.

CAN Bit Time - Nominal Bit Timimg: The NBT, or tbit, is made up of non-overlapping segments, therefore, the NBT is represented as below
tbit = tsynchseg + tpropseg + tps1 + tps2
Associated with the NBT are the Sample Point, Synchronization Jump Width (SJW) and Information Processing Time (IPT).

[CAN Bit Time Segments.png]

1. CAN Bit Time - Nominal Bit Timimg - Synchronization Segment: The SyncSeg is the first segment in the NBT and is used to synchronize the nodes on the bus. Bit edges are expected to occur within the SyncSeg. This segment is fixed at 1TQ.

2. CAN Bit Time - Nominal Bit Timimg - Propagation Segment: The PropSeg exists to compensate for physical delays between nodes. The propagation delay is defined as twice the sum of the signal’s propagation time on the bus line, including the delays associated with the bus driver. The PropSeg is programmable from 1 - 8TQ.

3. CAN Bit Time - Nominal Bit Timimg - Phase Segment 1 And Phase Segment 2: PS1 and PS2 are used to compensate for edge phase errors on the bus. PS1 can be lengthened or PS2 can be shortened by resyncronization. PS1 is programmable from 1 - 8TQ and PS2 isprogrammable from 2 - 8TQ.

4. CAN Bit Time - Nominal Bit Timimg - Sample Point: The sample point is the point in the bit time in which the logic level is read and interpreted. The sample point is located at the end of PS1. The exception to this rule is, if the sample mode is configured to sample three times per bit. In this case, the bit is still sampled at the end of PS1, however, two additional samples are taken at one-half TQ intervals prior to the end of PS1 and the value of the bit is determined by a majority decision.

5. CAN Bit Time - Nominal Bit Timimg - Information Processing Time: The IPT is the time required for the logic to determine the bit level of a sampled bit. The IPT begins at the sample point, is measured in TQ and is typically fixed for vendor specific modules. Since PS2 also begins at the sample point and is the last segment in the bit time, it is required that PS2 minimum is not less than the IPT. Therefore: PS2min = IPT - vendor specific TQ.

6. CAN Bit Time - Nominal Bit Timimg - Synchronization Jump Width: SJW adjusts the bit clock as necessary by 1 - 4TQ (as configured) to maintain synchronization with the transmitted message.

7. CAN Bit Time - Nominal Bit Timimg - Time Quantum: Each of the segments that make up a bit time are madeup of integer units called TQ. The length of each TQ is based on the oscillator period. The base TQ equals twice the oscillator period.
TQ = 2 * Baud Rate Prescalar (BRP) * Tosc = 2 * BRP / Fosc

[Tq and Bit Period.png]

A Simple Application with CAN:
[1 Conventional Multi Wire Looms.jpg]
[2 Remote Starter Conventional Multi Wire Looms.jpg]
[3 replaced CAN of a car.jpg]
[4 replaced CAN Remote Starter of a car.jpg]

References:
https://www.engineersgarage.com/article_page/can-protocol-understanding-the-controller-area-network-protocol/
https://en.wikipedia.org/wiki/CAN_bus
https://www.elprocus.com/controller-area-network-can/
https://www.csselectronics.com/screen/page/simple-intro-to-can-bus/language/en
https://www.slideshare.net/mbedlabsTechnosoluti/can-bus-65612867
https://youtu.be/zQ1jK6OWsSM
http://course.ece.cmu.edu/~ece649/lectures/14_can.pdf
https://www.eecs.umich.edu/courses/eecs461/doc/CAN_notes.pdf
https://www.microchip.com/stellent/groups/SiteComm_sg/documents/DeviceDoc/en558265.pdf
http://www.port.de/cgi-bin/CAN/CanFaqErrors
https://www.cl.cam.ac.uk/research/srg/han/Lambda/webdocs/an713.pdf
https://www.nxp.com/docs/en/application-note/AN1798.pdf
http://ww1.microchip.com/downloads/en/appnotes/00754.pdf
https://canbuskits.com/what.php